package com.diginepal.sahakari.util;

import com.diginepal.sahakari.dto.JwtTokenDetails;
import com.diginepal.sahakari.service.CustomUserDetailsService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class JwtUtil {
    private static final String AUTHORITIES_KEY = "authorities";
    @Value("${digi.config.token.expiry.hour:10}")
    private int tokenExpiryHour;

    @Value("${digi.config.client.secret:DIGINEPALSECRETKEY}")
    private String secret;

    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    public void setCustomUserDetailsService(CustomUserDetailsService customUserDetailsService) {
        this.customUserDetailsService = customUserDetailsService;
    }

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public JwtTokenDetails generateToken(String username) {
        Map<String, Object> claims = new HashMap<>();

        UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);

        final String authorities = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));
        claims.put(AUTHORITIES_KEY, authorities);
        claims.put("username", userDetails.getUsername());

        return createToken(claims, username);
    }

    private JwtTokenDetails createToken(Map<String, Object> claims, String subject) {
        Date expiration = new Date(System.currentTimeMillis() + 1000 * 60 * 60 * tokenExpiryHour);
        Date issuedAt = new Date(System.currentTimeMillis());

        String token = Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(issuedAt)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS256, secret).compact();

        return new JwtTokenDetails((String) claims.get("username"), token, issuedAt.toString(), expiration.toString(), (String) claims.get(AUTHORITIES_KEY));
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

}
