package com.diginepal.sahakari.repository;

import com.diginepal.sahakari.entity.Matcher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MatcherRepository extends JpaRepository<Matcher, Integer> {
}
