package com.diginepal.sahakari.controller;

import com.diginepal.sahakari.dto.AuthenticationRequest;
import com.diginepal.sahakari.dto.JwtTokenDetails;
import com.diginepal.sahakari.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1")
public class WelcomeController {
    private JwtUtil jwtUtil;
    private AuthenticationManager authenticationManager;

    @Autowired
    public void setJwtUtil(JwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @GetMapping("/")
    public String welcome() {
        return "Welcome moderator to DigiNepal Sahakari System.";
    }

    @GetMapping("/admin")
    public String welcomeAdmin() {
        return "Welcome admin to DigiNepal Sahakari System.";
    }

    @PostMapping("/authenticate")
    public JwtTokenDetails generateToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(), authenticationRequest.getPassword()
                    )
            );
        } catch (Exception ex) {
            throw new Exception("Invalid username/password", ex.getCause());
        }
        return jwtUtil.generateToken(authenticationRequest.getUserName());
    }
}
