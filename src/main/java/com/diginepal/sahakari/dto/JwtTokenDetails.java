package com.diginepal.sahakari.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({
        "username",
        "token",
        "iat",
        "exp",
        "roles"
})
public class JwtTokenDetails {
    @JsonProperty("username")
    private String username;
    @JsonProperty("token")
    private String authToken;
    @JsonProperty("iat")
    private String issuedAt;
    @JsonProperty("exp")
    private String expirationAt;
    @JsonProperty("roles")
    private String roles;
}
