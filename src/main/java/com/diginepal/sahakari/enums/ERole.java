package com.diginepal.sahakari.enums;

import lombok.Getter;

@Getter
public enum ERole {
    ROLE_USER("USER"),
    ROLE_MODERATOR("MODERATOR"),
    ROLE_ADMIN("ADMIN"),
    ROLE_OTHER("OTHER");
    private final String value;

    ERole(String value) {
        this.value = value;
    }
}
