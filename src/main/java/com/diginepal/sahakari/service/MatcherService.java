package com.diginepal.sahakari.service;

import com.diginepal.sahakari.entity.Matcher;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import java.util.List;

public interface MatcherService {
    List<Matcher> getAll();
    void setMatcherToHttpSecurity(HttpSecurity http);
}
