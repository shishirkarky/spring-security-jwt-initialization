package com.diginepal.sahakari.service;

import com.diginepal.sahakari.entity.Matcher;
import com.diginepal.sahakari.enums.MatcherStatus;
import com.diginepal.sahakari.repository.MatcherRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class MatcherServiceImpl implements MatcherService {
    private MatcherRepository matcherRepository;

    @Autowired
    public void setMatcherRepository(MatcherRepository matcherRepository) {
        this.matcherRepository = matcherRepository;
    }

    @Override
    public List<Matcher> getAll() {
        return matcherRepository.findAll();
    }

    @Override
    public void setMatcherToHttpSecurity(HttpSecurity http) {
        List<Matcher> matcherList = getAll();

        matcherList.stream().filter(matcher -> matcher.getValue().equals(MatcherStatus.PERMIT_ALL.getValue())).forEach(matcher -> {
            try {
                http.authorizeRequests().antMatchers(matcher.getName()).permitAll();
            } catch (Exception e) {
                log.warn("mapping permitAll antMatchers exception occurred with error cause {}", e.getMessage());
            }
        });

        matcherList.stream().filter(matcher -> matcher.getValue().equals(MatcherStatus.AUTHORIZED.getValue())).forEach(matcher -> {
            try {
                http.authorizeRequests().antMatchers(matcher.getName()).hasRole(matcher.getRoleInfo());
            } catch (Exception e) {
                log.warn("antMatchers exception occurred with error cause {}", e.getMessage());
            }
        });
    }
}
