package com.diginepal.sahakari;

import com.diginepal.sahakari.entity.Matcher;
import com.diginepal.sahakari.entity.Roles;
import com.diginepal.sahakari.entity.User;
import com.diginepal.sahakari.enums.ERole;
import com.diginepal.sahakari.enums.MatcherStatus;
import com.diginepal.sahakari.repository.MatcherRepository;
import com.diginepal.sahakari.repository.RolesRepository;
import com.diginepal.sahakari.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.security.crypto.password.PasswordEncoder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
@EntityScan("com.diginepal.sahakari.entity")
public class SahakariApplication {
    private UserRepository userRepository;
    private RolesRepository rolesRepository;
    private MatcherRepository matcherRepository;
    private PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setRolesRepository(RolesRepository rolesRepository) {
        this.rolesRepository = rolesRepository;
    }

    @Autowired
    public void setMatcherRepository(MatcherRepository matcherRepository) {
        this.matcherRepository = matcherRepository;
    }

    @Autowired
    public void setbCryptPasswordEncoder(PasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostConstruct
    public void initUsers() {
        List<Roles> roles = Stream.of(
                new Roles(101, ERole.ROLE_ADMIN),
                new Roles(101, ERole.ROLE_USER)
        ).collect(Collectors.toList());
        Set<Roles> rolesSet = new HashSet<>(rolesRepository.saveAll(roles));

        List<User> users = Stream.of(
                new User(101, "shishir", bCryptPasswordEncoder.encode("password1"), "shishir@gmail.com", rolesSet),
                new User(102, "sunil", bCryptPasswordEncoder.encode("password2"), "sunil@gmail.com", rolesSet),
                new User(103, "ayusha", bCryptPasswordEncoder.encode("password3"), "ayusha@gmail.com", rolesSet)
        ).collect(Collectors.toList());
        userRepository.saveAll(users);

        List<Matcher> matchers = Stream.of(
                new Matcher(101, "/api/v1/admin", MatcherStatus.AUTHORIZED.getValue(), ERole.ROLE_ADMIN.getValue()),
                new Matcher(102, "/api/v1/", MatcherStatus.AUTHORIZED.getValue(), ERole.ROLE_MODERATOR.getValue()),
                new Matcher(102, "/api/v1/authenticate", MatcherStatus.PERMIT_ALL.getValue(), ERole.ROLE_OTHER.getValue()),
                new Matcher(102, "/swagger-ui.html", MatcherStatus.PERMIT_ALL.getValue(), ERole.ROLE_OTHER.getValue())
        ).collect(Collectors.toList());
        matcherRepository.saveAll(matchers);
    }

    public static void main(String[] args) {
        SpringApplication.run(SahakariApplication.class, args);
    }

}
